package com.youchy.android.trovitest.widget;

import com.youchy.android.trovitest.R;
import com.youchy.android.trovitest.image.PicassoTrustAll;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@EViewGroup(R.layout.view_user_details)
public class UserDetailsView extends RelativeLayout {

    @ViewById(R.id.image)
    ImageView mImageView;

    @ViewById(R.id.title)
    TextView mTextView;

    @ViewById(R.id.favorite)
    TextView mFavouriteTextView;

    private OnUserDetailActionListener listener;

    @Click(R.id.website)
    void onGoWebsite() {
        listener.onGoToWebsite();
    }

    @Click(R.id.favorite)
    void onFavorite() {
        setIsFavourite(!mFavouriteTextView.isSelected());
        listener.onFavourite(isFavourite());
    }

    public UserDetailsView(Context context) {
        super(context);
    }

    public UserDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UserDetailsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setImage(String url) {
        PicassoTrustAll.getInstance(getContext()).load(url).into(mImageView);
    }

    public void setName(String name) {
        mTextView.setText(name);
    }

    public void setIsFavourite(boolean fav) {
        mFavouriteTextView.setSelected(fav);
    }

    public boolean isFavourite() {
        return mFavouriteTextView.isSelected();
    }

    public interface OnUserDetailActionListener {

        void onGoToWebsite();

        void onFavourite(boolean isFavourite);
    }

    public void setOnUserDetailsActionListener(OnUserDetailActionListener listener) {
        this.listener = listener;
    }


}
