package com.youchy.android.trovitest.fragments;

import com.youchy.android.trovitest.R;
import com.youchy.android.trovitest.controller.UserController;
import com.youchy.android.trovitest.model.User;
import com.youchy.android.trovitest.widget.UserDetailsView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import android.app.Fragment;

@EFragment(R.layout.fragment_user_detail)
public class UserDetailsFragment extends Fragment {

    private static final String ARG_USER = "arg.user";

    @FragmentArg(ARG_USER)
    User user;

    @Bean
    UserController mUserController;

    @ViewById(R.id.user_details)
    UserDetailsView mUserDetailsView;

    @AfterViews
    void afterViews() {
        mUserDetailsView.setName(user.getAlias());
        mUserDetailsView.setImage(user.getAvatarUrl());
        mUserDetailsView.setIsFavourite(user.isFavourite());
        mUserDetailsView.setOnUserDetailsActionListener(new UserDetailsView.OnUserDetailActionListener() {
            @Override
            public void onGoToWebsite() {
                if (getActivity() != null) {
                    mUserController.viewWebsite(user);
                }
            }

            @Override
            public void onFavourite(boolean isFavourite) {
                if (getActivity() != null) {
                    user.setFavourite(isFavourite);
                    getArguments().putParcelable(ARG_USER, user);
                    markAsFavourite(user.getId(), isFavourite);
                }
            }
        });
    }

    @Background
    void markAsFavourite(int id, boolean fav) {
        mUserController.setUserAsFavourite(id, fav);
    }

}