package com.youchy.android.trovitest.event;

import com.youchy.android.trovitest.model.User;

import java.util.List;

public class UsersSearchResultEvent extends Event {

    private List<User> users;

    public UsersSearchResultEvent(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }
}
