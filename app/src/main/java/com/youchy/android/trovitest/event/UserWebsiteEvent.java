package com.youchy.android.trovitest.event;

import com.youchy.android.trovitest.model.User;

public class UserWebsiteEvent extends Event {

    private User user;

    public UserWebsiteEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
