package com.youchy.android.trovitest.controller;

import com.youchy.android.trovitest.api.ApiException;
import com.youchy.android.trovitest.api.UserGithubClient;
import com.youchy.android.trovitest.data.DataProvider;
import com.youchy.android.trovitest.data.DbDataProvider;
import com.youchy.android.trovitest.data.DbHelper;
import com.youchy.android.trovitest.event.ApiErrorEvent;
import com.youchy.android.trovitest.event.Event;
import com.youchy.android.trovitest.event.UserDetailsEvent;
import com.youchy.android.trovitest.event.UserWebsiteEvent;
import com.youchy.android.trovitest.event.UsersSearchResultEvent;
import com.youchy.android.trovitest.model.User;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SupposeBackground;
import org.androidannotations.annotations.UiThread;

import android.database.Cursor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.greenrobot.event.EventBus;

@EBean
public class UserController {

    @Bean
    UserGithubClient mApiClient;

    @Bean(DbDataProvider.class)
    DataProvider mDataProvider;

    @SupposeBackground
    public void setUserAsFavourite(int githubId, boolean fav) {
        mDataProvider.setFavorite(githubId, fav);
    }

    @SupposeBackground
    public void searchUsers(String text) {
        try {
            List<User> users = mApiClient.searchUsers(text);
            Map<Integer, User> githubIds = buildUsersMap(users);
            Set<Integer> set = githubIds.keySet();
            Cursor cursor = mDataProvider.getUsers(set.toArray(new Integer[set.size()]));
            mergeApiAndLocalUsers(githubIds, cursor);
            postEvent(new UsersSearchResultEvent(users));
        } catch (ApiException e) {
            postEvent(new ApiErrorEvent());
        }
    }

    @SupposeBackground
    public void visitUser(User user) {
        user.setVisited(true);
        postEvent(new UserDetailsEvent(user));
        user = mDataProvider.saveUser(user);
    }

    @UiThread
    void postEvent(Event e) {
        EventBus.getDefault().post(e);
    }

    public void viewWebsite(User user) {
        postEvent(new UserWebsiteEvent(user));
    }

    private Map<Integer, User> buildUsersMap(List<User> users) {
        Map<Integer, User> map = new HashMap<>();
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            map.put(user.getId(), user);
        }
        return map;
    }

    private void mergeApiAndLocalUsers(Map<Integer, User> githubIds, Cursor cursor) {
        if (cursor != null) {
            int idIndex = cursor.getColumnIndex(DbHelper.USER_GITHUBID);
            int favIndex = cursor.getColumnIndex(DbHelper.USER_FAV);

            while (cursor.moveToNext()) {
                int id = cursor.getInt(idIndex);
                boolean fav = cursor.getInt(favIndex) != 0;
                User user = githubIds.get(id);
                user.setVisited(true);
                user.setFavourite(fav);
            }
        }
    }
}
