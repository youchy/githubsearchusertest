package com.youchy.android.trovitest.event;

import com.youchy.android.trovitest.model.User;

public class UserDetailsEvent extends Event {

    private User user;

    public UserDetailsEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
