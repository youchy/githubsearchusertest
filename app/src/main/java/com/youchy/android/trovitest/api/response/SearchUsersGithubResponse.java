package com.youchy.android.trovitest.api.response;

import com.google.gson.annotations.SerializedName;

import com.youchy.android.trovitest.model.User;

import java.util.ArrayList;
import java.util.List;

public class SearchUsersGithubResponse {

    @SerializedName("items")
    private ArrayList<User> users;

    public List<User> getUsers() {
        return users;
    }
}
