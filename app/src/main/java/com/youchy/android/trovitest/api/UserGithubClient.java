package com.youchy.android.trovitest.api;

import com.youchy.android.trovitest.api.response.SearchUsersGithubResponse;
import com.youchy.android.trovitest.model.User;

import org.androidannotations.annotations.EBean;

import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.http.GET;
import retrofit.http.Query;


@EBean
public class UserGithubClient {

    private static final String GITHUB_ENDPOINT = "https://api.github.com/search";

    interface UserGithubService {

        @GET("/users?&page=1&per_page=20")
        SearchUsersGithubResponse getRecents(@Query("q") String search);
    }

    public List<User> searchUsers(String str) throws ApiException {
        RestAdapter restAdapter = new RestAdapter.Builder()
          .setEndpoint(GITHUB_ENDPOINT)
          .build();
        UserGithubService service = restAdapter.create(UserGithubService.class);
        try {
            SearchUsersGithubResponse response = service.getRecents(str);
            return response.getUsers();
        } catch (RetrofitError e) {
            throw new ApiException();
        }
    }

}