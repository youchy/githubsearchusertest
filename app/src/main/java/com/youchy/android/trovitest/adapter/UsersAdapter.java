package com.youchy.android.trovitest.adapter;

import com.youchy.android.trovitest.model.User;
import com.youchy.android.trovitest.widget.UserListItem;
import com.youchy.android.trovitest.widget.UserListItem_;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public class UsersAdapter extends BaseAdapter {

    private final List<User> items;

    public UsersAdapter(Context context, List<User> users) {
        this.items = users;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public User getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = UserListItem_.build(parent.getContext());
        }

        User item = getItem(position);
        ((UserListItem) convertView).setText(item.getAlias());
        ((UserListItem) convertView).setPhoto(item.getAvatarUrl());
        ((UserListItem) convertView).setVisited(item.isVisited());

        return convertView;
    }

    public void add(List<User> users) {
        items.addAll(users);
        notifyDataSetChanged();
    }

    public void swap(List<User> users) {
        items.clear();
        items.addAll(users);
        notifyDataSetChanged();
    }
}
