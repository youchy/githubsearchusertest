package com.youchy.android.trovitest.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("login")
    private String alias;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("html_url")
    private String siteUrl;

    private boolean favourite = false;

    private boolean visited = false;

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(alias);
        dest.writeString(avatarUrl);
        dest.writeString(siteUrl);
        dest.writeInt(favourite ? 1 : 0);
        dest.writeInt(visited ? 1 : 0);
    }

    public User() {
    }

    public User(Parcel in) {
        id = in.readInt();
        alias = in.readString();
        avatarUrl = in.readString();
        siteUrl = in.readString();
        favourite = in.readInt() == 1 ? true : false;
        visited = in.readInt() == 1 ? true : false;
    }

    public int getId() {
        return id;
    }

    public String getAlias() {
        return alias;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public User clone() {
        User user = new User();
        user.id = this.id;
        user.alias = this.alias;
        user.avatarUrl = this.avatarUrl;
        user.siteUrl = this.siteUrl;
        user.favourite = this.favourite;
        user.visited = this.visited;
        return user;
    }
}