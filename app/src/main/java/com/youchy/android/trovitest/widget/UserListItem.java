package com.youchy.android.trovitest.widget;

import com.youchy.android.trovitest.R;
import com.youchy.android.trovitest.image.PicassoTrustAll;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@EViewGroup(R.layout.view_user_item)
public class UserListItem extends RelativeLayout {

    @ViewById(R.id.title)
    TextView mTextView;

    @ViewById(R.id.image)
    ImageView mImageView;

    @ViewById(R.id.visited)
    View mVisitedView;

    public UserListItem(Context context) {
        super(context);
    }

    public UserListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UserListItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setText(String title) {
        mTextView.setText(title);
    }

    public void setPhoto(String url) {
        PicassoTrustAll.getInstance(getContext()).load(url).resizeDimen(R.dimen.list_image_size, R.dimen.list_image_size).centerCrop()
          .into(mImageView);
    }

    public void setVisited(boolean visited) {
        if (visited) {
            mVisitedView.setVisibility(View.VISIBLE);
        } else {
            mVisitedView.setVisibility(View.INVISIBLE);
        }
    }
}
