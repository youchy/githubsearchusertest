package com.youchy.android.trovitest;

import com.youchy.android.trovitest.event.ApiErrorEvent;
import com.youchy.android.trovitest.event.UserDetailsEvent;
import com.youchy.android.trovitest.event.UserWebsiteEvent;
import com.youchy.android.trovitest.fragments.UserDetailsFragment;
import com.youchy.android.trovitest.fragments.UserDetailsFragment_;
import com.youchy.android.trovitest.fragments.UsersSearchFragment_;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;

import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@WindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS)
@EActivity(R.layout.activity_search_users)
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
              .add(R.id.container, UsersSearchFragment_.builder().build())
              .commit();
        }
    }

    public void onEvent(UserDetailsEvent event) {
        UserDetailsFragment fragment = UserDetailsFragment_.builder().user(event.getUser()).build();
        getFragmentManager().beginTransaction()
          .add(R.id.container, fragment)
          .addToBackStack(null)
          .commit();
    }

    public void onEvent(UserWebsiteEvent event) {
        WebActivity_.intent(this).mUserUrl(event.getUser().getSiteUrl()).start();
    }

    public void onEvent(ApiErrorEvent event) {
        Crouton.showText(this, R.string.error_search, Style.ALERT);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
