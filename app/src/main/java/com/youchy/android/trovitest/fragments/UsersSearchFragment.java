package com.youchy.android.trovitest.fragments;

import com.youchy.android.trovitest.R;
import com.youchy.android.trovitest.adapter.UsersAdapter;
import com.youchy.android.trovitest.controller.UserController;
import com.youchy.android.trovitest.event.ApiErrorEvent;
import com.youchy.android.trovitest.event.UsersSearchResultEvent;
import com.youchy.android.trovitest.model.User;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Fragment;
import android.os.Handler;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_search_users)
public class UsersSearchFragment extends Fragment {

    @ViewById(android.R.id.list)
    ListView mListView;

    @ViewById(R.id.user_search)
    EditText mSearchEditText;

    @ViewById(android.R.id.empty)
    TextView mEmptyView;

    @ViewById(R.id.loading)
    View mLoadingView;

    @Bean
    UserController mUserController;

    private Handler mHandler = new Handler();

    private UsersAdapter mAdapter;

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        hideProgress();
    }

    @AfterViews
    void afterViews() {
        mListView.setEmptyView(mEmptyView);
    }

    @ItemClick
    public void listItemClicked(User clickedItem) {
        clickedItem.setVisited(true);
        mAdapter.notifyDataSetChanged();
        visitUser(clickedItem.clone());
    }

    @Background
    void searchUsers(String text) {
        mUserController.searchUsers(text);
    }

    @AfterTextChange(R.id.user_search)
    void afterTextChangedOnUserSearch(final Editable text, TextView hello) {
        showProgress();
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                searchUsers(text.toString());
            }
        }, 500);
    }

    @Background
    void visitUser(User user) {
        mUserController.visitUser(user);
    }

    @UiThread
    public void onEvent(UsersSearchResultEvent event) {
        if (getActivity() != null) {
            hideProgress();
            getActivity().setProgressBarIndeterminateVisibility(false);
            if (mAdapter == null) {
                mAdapter = new UsersAdapter(getActivity(), event.getUsers());
                mListView.setAdapter(mAdapter);
            } else {
                mAdapter.swap(event.getUsers());
            }
        }
    }

    @UiThread
    public void onEvent(ApiErrorEvent event) {
        hideProgress();
    }

    void showProgress() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    void hideProgress() {
        mLoadingView.setVisibility(View.GONE);
    }

}